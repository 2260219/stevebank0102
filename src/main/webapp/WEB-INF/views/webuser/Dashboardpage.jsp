<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    
    <center>	
    	<!-- 통장이 없을경우 -->
	    <c:if test="${acctinfo eq null }">
	    	<h3>----통장 개설 준비중-----</h3>
	    </c:if>
	    
	    <!-- 통장이 있을경우 -->
	    <c:if test="${acctinfo ne null }">
			계좌번호 : ${acctinfo.acct_no }<br> 회원정보 : ${acctinfo.id }<br> 잔액 : ${acctinfo.balance }원
	    	<br>
	    	<a href="./plus">입금</a>
	    	<a href="./minus">출금</a>
	    	<a href="./trans">계좌이체</a>
	    	<a href="./loanregister">대출신청</a>
	    	
	    
	    </c:if>
    </center>