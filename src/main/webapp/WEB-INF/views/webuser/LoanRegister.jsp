<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<center>	
	<form action="./loanregister" method="post">
		<table border="1">
			<tr>
				<td>회원정보</td>
				<td><label>${acctinfo.id }</label></td>
			</tr>
			<tr>
				<td>회원계좌</td>
				<td><label>${acctinfo.acct_no }</label></td>
			</tr>
			<tr>
				<td>대출회사</td>
				<td>
				    	<select id="prtcode" name = "prt_code">
	    					<option value="햇살론">햇살론</option>
	    					<option value="보금자리론">보금자리론</option>
	    				</select>
				</td>
			</tr>
			<tr>
				<td>대출금액</td>
				<td><input type="number" name="loan_total"><td>
			</tr>
			<tr>
				<td>대출기간</td>
				<td><input type="number" name="loan_month"><td>
			</tr>	
			<tr>
				<td colspan="2"><input type="file" accept=".pdf" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="신청"></td>
			</tr>		
		</table>
	</form>
</center>