<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
  <script>
  function proc()
	{
	    var Acct_no = document.getElementById("acct_no").value;
		var Balance = document.getElementById("balance").value;
		var BankCode = document.getElementById("bankid").value;
		var BankCode2 = document.getElementById("bankid2").value;
		
		$.ajax({
			url: "./trans",
			type: "POST",
			data: {
				'acct_no' : Acct_no,
				'balance' : Balance,
				'comm_code' : BankCode,
				'comm_code2' : BankCode2
			},
			datatype: 'json',
			success: function(data)
			{
				console.log(data);
				if(data=="fail")
					{
					alert("정보가 없어요");
					return ;
				}
				else
					{
					
						if(confirm(data+"님이 맞습니까?"))
							{
							
							$.ajax({
								url: "./trans2",
								type: "POST",
								data: {
									'acct_no' : Acct_no,
									'balance' : Balance,
									'comm_code' : BankCode,
									'comm_code2' : BankCode2
								},
								datatype: 'json',
								success: function(data)
								{
									console.log(data);
									if(data=="fail")
										{
										alert("실패했어요");
										return ;
									}
									else
										{
										
											alert("성공했어요");
											return ;
										
										}
									
								}
								});
							
							
							}
						else
							{
								console.log("취소해버려");
							}
						return ;
					
					}
				
			}
			});
		
		
	}
  
  </script> 
  
  
    <center>   
    <form action="" method="">
    	<table>
    		  	 <tr>
	    			<td>
	    				<select id="bankid2" name="comm_code2" >
	    					<option value="당행이체">당행이체</option>
	    					<option value="타행이체">타행이체</option>
	    				</select>
	    			</td>
	    		</tr>
    		<tr>
    			<td>
    				<select id="bankid" name = "comm_code">
    					<option value="스티브뱅크">스티브뱅크</option>
    					<option value="우리은행">우리은행</option>
    					<option value="국민은행">국민은행</option>
    					<option value="기업은행">기업은행</option>
    					<option value="SC제일은행">SC제일은행</option>
    					<option value="우체국은행">기업은행</option>
    					<option value="신협은행">신협은행</option>
    					<option value="카카오뱅크">카카오뱅크</option>
    					<option value="농협은행">농협은행</option>
    					<option value="하나은행">하나은행</option>
    				</select>
    			</td>
    		</tr>
			<tr>
				<td><input type="text" id="acct_no" name="acct_no" placeholder="계좌번호 입력"></td>
			</tr>
			<tr>
				<td><input type="text" id="balance" name="balance" placeholder="금액 입력"></td>
			</tr>
			<tr>
				<td><input type="button" value="계좌이체" onclick="proc()"></td>
			</tr>    	
    	</table>
    </form>
</center>

