<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
  <!-- 
  <script src="resources/jquery-3.3.1.min.js"></script>
  -->
	<script>
		var Btnchk = new String("false"); // 입력체크
		
		// 아이디 중복체크 Ajax
		function id_overlap_check()
		{		
			
			id_overlap_input=document.querySelector('.username_input');
			if($('.username_input').val()=='')
				{
					$('.idchecklabel').text('메일을 입력하세요');
					$('.idchecklabel').css('color','red');
				}
			else{
			$.ajax({
				url: "./check",
				type: "POST",
				data: {
					'username' : id_overlap_input.value
				},
				datatype: 'json',
				success: function(data)
				{
					console.log(data);
					if(data=="fail")
						{
							Btnchk=new String("flase");
							$('.registerBtn').attr("disabled","disabled");
							$('.idchecklabel').text('이미 존재하는 아이디');
							$('.idchecklabel').css('color','red');
							id_overlap_input.focus();
							return;
						}
					else
						{
							Btnchk=new String("true");
							if($('.inputpwd').val()==$('.inputpwd2').val() && $('.inputpwd').val()!='' && $('.inputpwd2').val()!='' )
								{
									$('.registerBtn').removeAttr("disabled");
								}
							$('.idchecklabel').text('사용 가능한 아이디');
							$('.idchecklabel').css('color','green');
							$('.username_input').attr("check_result","success");
							return;
						}
				}
				});
			}
		}
		
		// 패스워드 확인처리
		function pw_check()
		{
			if($('.inputpwd').val()=='' && $('.inputpwd2').val()=='')
				{
					$('.pwdchecklabel').text("패스워드를 입력하세요");
					$('.pwdchecklabel').css('color','red');
				}
			else if($('.inputpwd').val()==$('.inputpwd2').val())
				{ 
					//아이디도 중복확인이 된다면 버튼 활성화
					if(Btnchk=="true")
						{
							$('.registerBtn').removeAttr("disabled");
						}
					$('.pwdchecklabel').text("패스워드가 일치합니다.");
					$('.pwdchecklabel').css('color','green');
				
				}
			else
				{
					$('.registerBtn').attr("disabled","disabled");
					$('.pwdchecklabel').text("패스워드가 일치하지 않습니다.");
					$('.pwdchecklabel').css('color','red');
				}
			
		}
			
		
		</script>

<center>	
<form action="./join" method="post">
<table  style="text-align:center;">
<tr>
	<td>아이디</td>
	<td>
		<input type="email" placeholder="email을 입력하세요" autofocus class="username_input" oninput="id_overlap_check()"  name="id">
	</td>
</tr>
<tr>
	<td colspan="2">
		<label class="idchecklabel" check_result="fail" ></label>
	</td>
</tr>
<tr>
	<td>패스워드</td>
	<td><input type="password" placeholder="패스워드 입력" class="inputpwd" name="pw" oninput="pw_check()"></td>
	<td rowspan="2"></td>
</tr>
<tr>
	<td>패스워드확인</td>
	<td ><input type="password" placeholder="패스워드 확인" class="inputpwd2" name="pwd2" oninput="pw_check()"></td>
</tr>
<tr>
	<td colspan="2">
		<label class="pwdchecklabel" ></label>
	</td>
</tr>
<tr>
	<td>이름</td>
	<td colspan="2"><input type="text" placeholder="이름 입력" name="name"></td>
</tr>
<tr>
	<td>주민등록번호</td>
	<td colspan="2"><input type="text" placeholder="앞 8자리만 입력해주세요." name="sn"></td>
</tr>
<tr>
	<td>주소</td>
	<td colspan="2"><input type="text" placeholder="주소 입력" name="address"></td>
</tr>
<tr>
	<td>유저타입</td>
	<td colspan="2"><input type="text" placeholder="직원은 c 일반은 e" name="type"></td>
</tr>
<tr>
	
	<td colspan="3"><br><input type="submit" class="btn btn-info btn-block registerBtn" disabled value="가입"><br></td>
	
</tr>
</table>
</form>


</center>

