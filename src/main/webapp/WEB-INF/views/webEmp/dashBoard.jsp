<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h1>dashBoard.jsp</h1>

<div class="container" style="margin: 30px 0;">

	<div class="row" style="margin: 10% 0;">
		<div class="col-sm">
			<div class="alert alert-success" role="alert">
				<h2 class="alert-heading">통장 미개설 고객 /<br /> 승인대기 대출</h2>
				<p>기간에 관계 없이 통장 미개설 고객 및 미승인된 대출 총합</p>
				<hr>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						통장 미개설 고객 <span class="badge badge-primary badge-pill">${acctCount}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						승인대기 대출<span class="badge badge-primary badge-pill">${approvalCount}</span>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-sm">
			<div class="alert alert-success" role="alert">
				<h2 class="alert-heading">개설된 통장 /<br /> 승인된 대출</h2>
				<p>기간에 관계 없이 개설 및 승인된 대출과 통장 총합</p>
				<hr>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						개설된 통장 <span class="badge badge-primary badge-pill">${getAcctComCount}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						승인된 대출 <span class="badge badge-primary badge-pill">${getApprovalComList}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row" style="margin: 10% 0;">
		<div class="col-sm">
			<div class="alert alert-warning" role="alert">
				<h2 class="alert-heading">기간별 가입 회원</h2>
				<p>하루/일주일/한달간 가입한 회원</p>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						하루동안 가입한 회원<span class="badge badge-primary badge-pill">${listData["getUserlistDay"]}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						지난 일주일간 가입한 회원<span class="badge badge-primary badge-pill">${listData["getUserlistWeek"]}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						지난 한달간 가입한 회원 <span class="badge badge-primary badge-pill">${listData["getUserlistMonth"]}</span>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-sm">
			<div class="alert alert-warning" role="alert">
				<h2 class="alert-heading">기간별 개설 통장</h2>
				<p>하루/일주일/한달간 개설된 통장</p>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						하루동안 개설된 통장<span class="badge badge-primary badge-pill">${listData["getAcctlistDay"]}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						지난 일주일동안 개설된 통장<span class="badge badge-primary badge-pill">${listData["getAcctlistWeek"]}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						지난 한달간 개설된 통장 <span class="badge badge-primary badge-pill">${listData["getAcctlistMonth"]}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>


	<div class="row" style="margin: 0;">
		<div class="col-sm">
			<div class="alert alert-dark" role="alert">
				<h2 class="alert-heading">기간별 발생 대출</h2>
				<p>하루/일주일/한달간 발생된 대출</p>
				<ul class="list-group">
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						하루동안 발생된 대출<span class="badge badge-primary badge-pill">${listData["getLoanlistDay"]}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						지난 일주일동안 발생된 대출<span class="badge badge-primary badge-pill">${listData["getLoanlistWeek"]}</span>
					</li>
					<li
						class="list-group-item d-flex justify-content-between align-items-center">
						지난 한달간 발생된 대출 <span class="badge badge-primary badge-pill">${listData["getLoanlistMonth"]}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

</div>