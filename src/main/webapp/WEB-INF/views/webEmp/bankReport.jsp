<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h1>bankReport.jsp</h1>

<style>
.highcharts-figure, .highcharts-data-table table {
	min-width: 310px;
	max-width: 1180px;
	margin: 1em auto;
}

#container {
	height: 720px;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 800px;
}

.highcharts-data-table caption {
	padding: 1em 0;
	font-size: 1.2em;
	color: #555;
}

.highcharts-data-table th {
	font-weight: 600;
	padding: 0.5em;
}

.highcharts-data-table td, .highcharts-data-table th,
	.highcharts-data-table caption {
	padding: 0.5em;
}

.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even)
	{
	background: #f8f8f8;
}

.highcharts-data-table tr:hover {
	background: #f1f7ff;
}
</style>

<figure class="highcharts-figure">
	<div id="container"></div>
	<p class="highcharts-description"></p>
</figure>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<script type="text/javascript">
	const acctCnt = [];
	const loanCnt = [];
	const loanSum = [];
	const bankSum = [];

	<c:forEach var="row" items="${stat}">
	acctCnt.push(Number("${row.acct_cnt}"));
	loanCnt.push(Number("${row.loan_cnt}"));
	loanSum.push(Number("${row.loan_sum}"));
	bankSum.push(Number("${row.bank_sum}"));
	</c:forEach>

	Highcharts
			.chart(
					'container',
					{
						chart : {
							type : 'column'
						},
						title : {
							text : '월별 통계'
						},
						subtitle : {
							text : '통장개설건수, 대출건수, 대출총액, 은행보유액'
						},
						xAxis : {
							categories : [ '9월', '10월', '11월', '11월', '12월' ],
							crosshair : true
						},
						yAxis : {
							min : 0,
							title : {
								text : 'acct_cnt, loan_cnt, loan_sum, bank_sum'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '통장개설건수',
									data : [ acctCnt[0], acctCnt[1],
											acctCnt[2], acctCnt[3], acctCnt[4] ]

								},
								{
									name : '대출건수',
									data : [ loanCnt[0], loanCnt[1],
											loanCnt[2], loanCnt[3], loanCnt[4] ]

								},
								{
									name : '대출총액',
									data : [ loanSum[0], loanSum[1],
											loanSum[2], loanSum[3], loanSum[4] ]

								},
								{
									name : '은행보유액',
									data : [ bankSum[0], bankSum[1],
											bankSum[2], bankSum[3], bankSum[4] ]

								} ]
					});
</script>

