<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
function fn_paging(curPage) {
	location.href = "/stevebank/loanDetail?curPage=" + curPage;
}
</script>
 
<h1 style="margin: 30px 0;">loanDetail.jsp</h1>

<div style="margin:12px 0;">
	<button type="button" class="btn btn-primary">
	 승인된 대출 <span class="badge badge-light">${pagination.listCnt}</span>
	</button>
</div>

<table class="table table-bordered">
	<tbody>
	<c:forEach var="row" items="${getComApproval}">
			<tr class="bg-dark">
				<td colspan="8"></td>
			</tr>
			<tr>
				<th class="bg-warning">아이디</th>
				<td>${row.id}</td>
				<th class="bg-warning">계좌번호</th>
				<td>${row.acct_no}</td>
				<th class="bg-warning">대출신청액</th>
				<td>${row.loan_total}</td>
				<th class="bg-warning">대출개월수</th>
				<td>${row.loan_month}</td>
			</tr>
			<tr>
				<th class="bg-warning">월상환액</th>
				<td>${row.loan_amount}</td>
				<th class="bg-warning">상품코드</th>
				<td>${row.loan_fee}</td>
				<th class="bg-warning">남은금액</th>
				<td>${row.loan_balance}</td>
				<th class="bg-warning">연체일</th>
				<td>${row.loan_rate}</td>
			</tr>
			<tr>
				<th class="bg-warning">연체횟수</th>
				<td>${row.delay_cnt}</td>
				<th class="bg-warning">대출신청일</th>
				<td>${row.loan_ask}</td>
				<th class="bg-warning">대출승인일</th>
				<td>${row.acct_dt}</td>
				<th class="bg-warning">일련번호</th>
				<td>${row.loan_seq}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<nav aria-label="Page navigation example">
	<ul class="pagination justify-content-center">
		<c:if test="${pagination.curRange ne 1 }">
			<li class="page-item">
				<a onClick="fn_paging(1)" class="page-link" href="#" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
		</c:if>
		<c:if test="${pagination.curPage ne 1}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.prevPage }')" class="page-link" href="#">[이전]</a>
			</li>
		</c:if>

		<c:forEach var="pageNum" begin="${pagination.startPage}" end="${pagination.endPage}">
			<c:choose>
				<c:when test="${pageNum eq pagination.curPage}">
					<li class="page-item active" aria-current="page">
						<a class="page-link" onClick="fn_paging('${pageNum }')">${pageNum }</a>
						<span class="sr-only">(current)</span>
					</li>
				</c:when>
				<c:otherwise>
					<li class="page-item">
						<a class="page-link" href="#" onClick="fn_paging('${pageNum}')">${pageNum }</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<c:if test="${pagination.curPage ne pagination.pageCnt && pagination.pageCnt > 0}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.nextPage }')" class="page-link" href="#">[다음]</a>
			</li>
		</c:if>
		<c:if test="${pagination.curRange ne pagination.rangeCnt && pagination.rangeCnt > 0}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.pageCnt }')" class="page-link" href="#" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</c:if>
	</ul>
</nav>