<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
function fn_paging(curPage) {
	location.href = "/stevebank/acctCreate?curPage=" + curPage;
}
</script>

<h1>acctCreate.jsp</h1>

<div style="margin:12px 0;">
	<button type="button" class="btn btn-primary">
	 통장 미개설 <span class="badge badge-light">${pagination.listCnt}</span>
	</button>
</div>

<table class="table table-bordered">
	<thead>
		<tr class="bg-warning">
			<th scope="col">아이디</th>
			<th scope="col">이름</th>
			<th scope="col">생년월일</th>
			<th scope="col">주소</th>
			<th scope="col">개설</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="row" items="${acctList}">
			<tr>
				<td>${row.id}</td>
				<td>${row.name}</td>
				<td>${row.sn}</td>
				<td>${row.addr}</td>
				<td class="acctCreateSubmit" data-toggle="modal" data-target="#acctModal" data-id="${row.id}"  data-name="${row.name}"  data-sn="${row.sn}"  data-addr="${row.addr}">
					<button type="button" class="btn btn-light">통장 개설</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<nav aria-label="Page navigation example">
	<ul class="pagination justify-content-center">
		<c:if test="${pagination.curRange ne 1 }">
			<li class="page-item">
				<a onClick="fn_paging(1)" class="page-link" href="#" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
		</c:if>
		<c:if test="${pagination.curPage ne 1}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.prevPage }')" class="page-link" href="#">[이전]</a>
			</li>
		</c:if>

		<c:forEach var="pageNum" begin="${pagination.startPage}" end="${pagination.endPage}">
			<c:choose>
				<c:when test="${pageNum eq pagination.curPage}">
					<li class="page-item active" aria-current="page">
						<a class="page-link" onClick="fn_paging('${pageNum }')">${pageNum }</a>
						<span class="sr-only">(current)</span>
					</li>
				</c:when>
				<c:otherwise>
					<li class="page-item">
						<a class="page-link" href="#" onClick="fn_paging('${pageNum}')">${pageNum }</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<c:if test="${pagination.curPage ne pagination.pageCnt && pagination.pageCnt > 0}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.nextPage }')" class="page-link" href="#">[다음]</a>
			</li>
		</c:if>
		<c:if test="${pagination.curRange ne pagination.rangeCnt && pagination.rangeCnt > 0}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.pageCnt }')" class="page-link" href="#" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</c:if>
	</ul>
</nav>

<div class="modal fade" id="acctModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">통장 생성</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">아이디</label> 
						<input type="text" class="form-control" id="id" readonly>
					</div>
					<div class="form-group">
						<label class="col-form-label">이름</label> 
						<input type="text" class="form-control" id="name" readonly>
					</div>
					<div class="form-group">
						<label class="col-form-label">주소</label> 
						<input type="text" class="form-control" id="addr" readonly>
					</div>
					<div class="form-group">
						<label class="col-form-label">생년월일</label> 
						<input type="text" class="form-control" id="sn" readonly>
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label">비밀번호</label>
						<textarea class="form-control" id="acctPw"></textarea>
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label">잔고</label>
						<textarea class="form-control" id="balance" readonly>0</textarea>
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label">지점코드</label>
						<textarea class="form-control" id="brchCode" readonly>201</textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="acctSave" class="btn btn-primary">통장생성하기</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">취소하기</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		
        $(".acctCreateSubmit").click(function () {
        	
            const id = $(this).data('id');
            const name = $(this).data('name');
            const sn = $(this).data('sn');
            const addr = $(this).data('addr');
            
            $(".modal-body #id").val(id);
            $(".modal-body #name").val(name);
            $(".modal-body #sn").val(sn);
            $(".modal-body #addr").val(addr);
        }),
        

		$("#acctSave").click(function() {

			const data = {
				"acct_no" : "",
				"acct_pw" : $("#acctPw").val(),
				"balance" : $("#balance").val(),
				"brch_code" : $("#brchCode").val(),
				"id" : $("#id").val()
			};

			$.ajax({
				type : "POST",
				async : false,
				url : "./acctCreate",
				contentType : 'application/json',
				data : JSON.stringify(data),
				datatype : "json",
				contentType : 'application/json; charset=utf-8',
				error : function(json) {
					alert("저장 과정에서 오류 발생");
					console.log(data);
				},
				success : function(data) {
					$("#acctModal").modal("toggle");
				},
				complete : function(data) {
					location.reload();
				}
			});

		});

	});
</script>