<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
function fn_paging(curPage) {
	location.href = "/stevebank/loanRecognition?curPage=" + curPage;
}
</script>
 
<h1 style="margin: 30px 0;">loanRecognition.jsp</h1>

<div style="margin:12px 0;">
	<button type="button" class="btn btn-primary">
	 미승인 대출 <span class="badge badge-light">${pagination.listCnt}</span>
	</button>
</div>

<table class="table table-bordered">
	<thead>
		<tr class="bg-success">
			<th scope="col">아이디</th>
			<th scope="col">계좌번호</th>
			<th scope="col">대출신청액</th>
			<th scope="col">대출개월수</th>
			<th scope="col">월상환액</th>
			<th scope="col">월상환수수료</th>
			<th scope="col">승인</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="row" items="${appr}">
			<tr>
				<td>${row.id}</td>
				<td>${row.acct_no}</td>
				<td>${row.loan_total}</td>
				<td>${row.loan_month}</td>
				<td>${row.loan_amount}</td>
				<td>${row.loan_fee}</td>
				<td class="loanSubmit" data-toggle="modal" data-target="#loanModal" data-id="${row.id}"  data-acctno="${row.acct_no}"  data-loantotal="${row.loan_total}">
					<button type="button" class="btn btn-light">대출 승인</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<nav aria-label="Page navigation example">
	<ul class="pagination justify-content-center">
		<c:if test="${pagination.curRange ne 1}">
			<li class="page-item">
				<a onClick="fn_paging(1)" class="page-link" href="#" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
		</c:if>
		<c:if test="${pagination.curPage ne 1}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.prevPage}')" class="page-link" href="#">[이전]</a>
			</li>
		</c:if>
		<c:forEach var="pageNum" begin="${pagination.startPage}" end="${pagination.endPage}">
			<c:choose>
				<c:when test="${pageNum eq pagination.curPage}">
					<li class="page-item active" aria-current="page">
						<a class="page-link" onClick="fn_paging('${pageNum}')">${pageNum}</a>
						<span class="sr-only">(current)</span>
					</li>
				</c:when>
				<c:otherwise>
					<li class="page-item">
						<a class="page-link" href="#" onClick="fn_paging('${pageNum}')">${pageNum}</a>
					</li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<c:if test="${pagination.curPage ne pagination.pageCnt && pagination.pageCnt > 0}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.nextPage}')" class="page-link" href="#">[다음]</a>
			</li>
		</c:if>
		<c:if test="${pagination.curRange ne pagination.rangeCnt && pagination.rangeCnt > 0}">
			<li class="page-item">
				<a onClick="fn_paging('${pagination.pageCnt}')" class="page-link" href="#" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</c:if>
	</ul>
</nav>

<div class="modal fade" id="loanModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">대출 승인</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">아이디</label> 
						<input type="text" class="form-control" id="id" readonly>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">계좌번호</label> 
						<input type="text" class="form-control" id="acctNo" readonly>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">대출신청액</label> 
						<input type="text" class="form-control" id="loanTotal" readonly>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="loanSave" class="btn btn-primary">승인</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		
        $(".loanSubmit").click(function () {
        	
            const id = $(this).data('id');
            const acctNo = $(this).data('acctno');
            const loanTotal = $(this).data('loantotal');
            
            $(".modal-body #id").val(id);
            $(".modal-body #acctNo").val(acctNo);
            $(".modal-body #loanTotal").val(loanTotal);
        }),
        
		$("#loanSave").click(function() {

			const data = {
				"acctNo" : $(".modal-body #acctNo").val(),
				"loanTotal" : $(".modal-body #loanTotal").val()
			};

			$.ajax({
				type : "POST",
				async : false,
				url : "./loanRecognition",
				contentType : 'application/json',
				data: JSON.stringify(data),
				datatype : "json",
				contentType : 'application/json; charset=utf-8',
				error : function(json) {
					alert("저장 과정에서 오류 발생");
					console.table(data);
				},
				success : function(data) {
					$("#loanModal").modal("toggle");
				},
				complete : function(data) {
					location.reload();
				}
			});

		});

	});
</script>