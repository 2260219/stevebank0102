<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<tiles:insertAttribute name="webEmpHeader" />
<div id="webEmpNavi" style="margin:0 0 40px 0;">
	<tiles:insertAttribute name="webEmpNavi" />
</div>
<div id="webEmpContainer" class="container" style="height: 100vmin;">
	<tiles:insertAttribute name="webEmpContent" />
</div>
	<tiles:insertAttribute name="webEmpFooter" />

 	