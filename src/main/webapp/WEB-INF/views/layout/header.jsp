<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<center>

	<span>
	 	<a href="./">HEADER</a>
		<c:if test="${loginCheck eq null}">
				로그인	
		</c:if>
		<c:if test="${loginCheck ne null}">
				${loginCheck.name} 님 안녕하세요 <a href="./logout">로그아웃</a>	
		</c:if>
	</span>

</center>