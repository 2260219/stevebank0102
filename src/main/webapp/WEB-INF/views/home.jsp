<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
 <div id="navi">
    <tiles:insertAttribute name="navi" />
    </div>
<div id="header">
    <tiles:insertAttribute name="header" />
    </div>
    <div id="container">
       <tiles:insertAttribute name="content" />
    </div>
    <div id="footer">
    <tiles:insertAttribute name="footer" />
    </div> 
</body>
</html>

