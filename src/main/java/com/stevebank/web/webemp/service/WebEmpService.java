package com.stevebank.web.webemp.service;

import java.util.List;

import com.stevebank.web.webemp.dto.AcctCreateDto;
import com.stevebank.web.webemp.dto.AcctListDto;
import com.stevebank.web.webemp.dto.ApprovalDto;
import com.stevebank.web.webemp.dto.RecognitionDto;
import com.stevebank.web.webemp.dto.StatisticsDto;

public interface WebEmpService {

	public List<StatisticsDto> getStatData() throws Exception;

	public void acctCreate(AcctCreateDto acctData) throws Exception;

	public List<ApprovalDto> getAllApproval() throws Exception;
	
	public List<ApprovalDto> getApprovalData(ApprovalDto approvalDto) throws Exception;

	public List<AcctListDto> getAcctCount() throws Exception;

	public List<AcctListDto> getAcctList(AcctListDto acctListDto) throws Exception;
	
	public int getAcctComList() throws Exception;

	public int getApprovalComList() throws Exception;

	public List<ApprovalDto> getComApproval(AcctListDto acctListDto) throws Exception;

	public int getUserlistDay() throws Exception;

	public int getUserlistWeek() throws Exception;

	public int getUserlistMonth() throws Exception;

	public int getLoanlistDay() throws Exception;

	public int getLoanlistWeek() throws Exception;

	public int getLoanlistMonth() throws Exception;

	public int getAcctlistDay() throws Exception;

	public int getAcctlistWeek() throws Exception;

	public int getAcctlistMonth() throws Exception;
	
	/* [대출 처리] */	
	public void depositMoney(RecognitionDto data) throws Exception;

	public void aprovalComplete(RecognitionDto data) throws Exception;
	
}