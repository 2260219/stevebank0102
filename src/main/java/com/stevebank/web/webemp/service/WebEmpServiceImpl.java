package com.stevebank.web.webemp.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stevebank.web.webemp.dao.WebEmpDao;
import com.stevebank.web.webemp.dto.AcctCreateDto;
import com.stevebank.web.webemp.dto.AcctListDto;
import com.stevebank.web.webemp.dto.ApprovalDto;
import com.stevebank.web.webemp.dto.RecognitionDto;
import com.stevebank.web.webemp.dto.StatisticsDto;

@Service
public class WebEmpServiceImpl implements WebEmpService {

	@Autowired
	private SqlSession session;

	@Override
	public List<StatisticsDto> getStatData() throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		List<StatisticsDto> result = webempdao.getStatData();

		return result;
	}

	@Override
	public void acctCreate(AcctCreateDto acctData) throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		webempdao.acctCreate(acctData);
	}

	@Override
	public List<AcctListDto> getAcctList(AcctListDto acctListDto) throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		List<AcctListDto> result = webempdao.getAcctList(acctListDto);

		return result;
	}

	@Override
	public List<ApprovalDto> getAllApproval() throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		List<ApprovalDto> result = webempdao.getAllApproval();

		return result;
	}

	@Override
	public List<ApprovalDto> getApprovalData(ApprovalDto approvalDto) throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		List<ApprovalDto> result = webempdao.getApprovalData(approvalDto);

		return result;
	}


	@Override
	public List<AcctListDto> getAcctCount() throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		List<AcctListDto> result = webempdao.getAcctCount();

		return result;
	}

	@Override
	public int getAcctComList() throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getAcctComList();

		return result;
	}

	@Override
	public int getApprovalComList() throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getApprovalComList();

		return result;
	}

	@Override
	public List<ApprovalDto> getComApproval(AcctListDto acctListDto) throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		List<ApprovalDto> result = webempdao.getComApproval(acctListDto);

		return result;
	}

	@Override
	public int getUserlistDay() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getUserlistDay();

		return result;
	}

	@Override
	public int getUserlistWeek() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getUserlistWeek();

		return result;
	}

	@Override
	public int getUserlistMonth() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getUserlistMonth();

		return result;
	}

	@Override
	public int getLoanlistDay() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getLoanlistDay();

		return result;
	}

	@Override
	public int getLoanlistWeek() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getLoanlistWeek();

		return result;
	}

	@Override
	public int getLoanlistMonth() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getLoanlistMonth();

		return result;
	}

	@Override
	public int getAcctlistDay() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getAcctlistDay();

		return result;
	}

	@Override
	public int getAcctlistWeek() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getAcctlistWeek();

		return result;
	}

	@Override
	public int getAcctlistMonth() throws Exception {
		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		int result = webempdao.getAcctlistMonth();

		return result;
	}

	// [대출 처리]
	@Override
	public void aprovalComplete(RecognitionDto data) throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		webempdao.approvalComplete(data);
	}
	
	@Override
	public void depositMoney(RecognitionDto data) throws Exception {

		WebEmpDao webempdao = session.getMapper(WebEmpDao.class);
		webempdao.depositMoney(data);
	}

}
