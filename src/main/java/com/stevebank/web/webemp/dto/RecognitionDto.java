package com.stevebank.web.webemp.dto;

public class RecognitionDto {
	private int acctNo;
	private int loanTotal;

	public int getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(int acctNo) {
		this.acctNo = acctNo;
	}

	public int getLoanTotal() {
		return loanTotal;
	}

	public void setLoanTotal(int loanTotal) {
		this.loanTotal = loanTotal;
	}
}
