package com.stevebank.web.webemp.dto;

public class AcctCreateDto {

	private int acct_no; // 계좌번호
	private int acct_pw; // 비밀번호
	private int brch_code; // 지점번호
	private int balance; // 잔액
	private String id; // 아이디

	public int getAcct_no() {
		return acct_no;
	}

	public void setAcct_no(int acct_no) {
		this.acct_no = acct_no;
	}

	public int getAcct_pw() {
		return acct_pw;
	}

	public void setAcct_pw(int acct_pw) {
		this.acct_pw = acct_pw;
	}

	public int getBrch_code() {
		return brch_code;
	}

	public void setBrch_code(int brch_code) {
		this.brch_code = brch_code;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
