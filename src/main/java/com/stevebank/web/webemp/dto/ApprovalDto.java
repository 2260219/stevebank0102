package com.stevebank.web.webemp.dto;

public class ApprovalDto {

	private int loan_seq;
	private int acct_no;
	private String prt_code;
	private int loan_total;
	private int loan_month;
	private int loan_balance;
	private int loan_amount;
	private int loan_fee;
	private float loan_rate;
	private float monthly_payment;
	private int delay_cnt;
	private String dead_dt;
	private String acct_dt;
	private String id;
	private String acct_Seq;
	private String approval;
	private String loan_ask;
	private int index;
	private int pageSize;

	public ApprovalDto() {}

	public ApprovalDto(int index, int pageSize) {
		setIndex(index);
		setPageSize(pageSize);
	}

	public int getLoan_seq() {
		return loan_seq;
	}

	public void setLoan_seq(int loan_seq) {
		this.loan_seq = loan_seq;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getAcct_no() {
		return acct_no;
	}

	public void setAcct_no(int acct_no) {
		this.acct_no = acct_no;
	}

	public String getPrt_code() {
		return prt_code;
	}

	public void setPrt_code(String prt_code) {
		this.prt_code = prt_code;
	}

	public int getLoan_total() {
		return loan_total;
	}

	public void setLoan_total(int loan_total) {
		this.loan_total = loan_total;
	}

	public int getLoan_month() {
		return loan_month;
	}

	public void setLoan_month(int loan_month) {
		this.loan_month = loan_month;
	}

	public int getLoan_balance() {
		return loan_balance;
	}

	public void setLoan_balance(int loan_balance) {
		this.loan_balance = loan_balance;
	}

	public int getLoan_amount() {
		return loan_amount;
	}

	public void setLoan_amount(int loan_amount) {
		this.loan_amount = loan_amount;
	}

	public int getLoan_fee() {
		return loan_fee;
	}

	public void setLoan_fee(int loan_fee) {
		this.loan_fee = loan_fee;
	}

	public float getLoan_rate() {
		return loan_rate;
	}

	public void setLoan_rate(float loan_rate) {
		this.loan_rate = loan_rate;
	}

	public float getMonthly_payment() {
		return monthly_payment;
	}

	public void setMonthly_payment(float monthly_payment) {
		this.monthly_payment = monthly_payment;
	}

	public int getDelay_cnt() {
		return delay_cnt;
	}

	public void setDelay_cnt(int delay_cnt) {
		this.delay_cnt = delay_cnt;
	}

	public String getDead_dt() {
		return dead_dt;
	}

	public void setDead_dt(String dead_dt) {
		this.dead_dt = dead_dt;
	}

	public String getAcct_dt() {
		return acct_dt;
	}

	public void setAcct_dt(String acct_dt) {
		this.acct_dt = acct_dt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAcct_Seq() {
		return acct_Seq;
	}

	public void setAcct_Seq(String acct_Seq) {
		this.acct_Seq = acct_Seq;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getLoan_ask() {
		return loan_ask;
	}

	public void setLoan_ask(String loan_ask) {
		this.loan_ask = loan_ask;
	}

}
