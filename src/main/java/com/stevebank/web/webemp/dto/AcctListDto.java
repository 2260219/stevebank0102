package com.stevebank.web.webemp.dto;

public class AcctListDto {

	private String id;
	private String name;
	private String sn;
	private String addr;
	private int index;
	private int pageSize;

	public AcctListDto() {}

	public AcctListDto(int index, int pageSize) {
		setIndex(index);
		setPageSize(pageSize);
	}

	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}
}
