package com.stevebank.web.webemp.dto;

public class StatisticsDto {

	private int acct_cnt;
	private int loan_cnt;
	private int loan_sum;
	private int bank_sum;

	public int getAcct_cnt() {
		return acct_cnt;
	}

	public void setAcct_cnt(int acct_cnt) {
		this.acct_cnt = acct_cnt;
	}

	public int getLoan_cnt() {
		return loan_cnt;
	}

	public void setLoan_cnt(int loan_cnt) {
		this.loan_cnt = loan_cnt;
	}

	public int getLoan_sum() {
		return loan_sum;
	}

	public void setLoan_sum(int loan_sum) {
		this.loan_sum = loan_sum;
	}

	public int getBank_sum() {
		return bank_sum;
	}

	public void setBank_sum(int bank_sum) {
		this.bank_sum = bank_sum;
	}

}
