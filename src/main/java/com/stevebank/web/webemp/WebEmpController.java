package com.stevebank.web.webemp;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stevebank.web.webemp.dto.AcctCreateDto;
import com.stevebank.web.webemp.dto.AcctListDto;
import com.stevebank.web.webemp.dto.ApprovalDto;
import com.stevebank.web.webemp.dto.RecognitionDto;
import com.stevebank.web.webemp.dto.StatisticsDto;
import com.stevebank.web.webemp.pagination.Pagination;
import com.stevebank.web.webemp.service.WebEmpServiceImpl;

@Controller
public class WebEmpController {

	@Autowired
	WebEmpServiceImpl WebEmpService;

	// [대시보드]
	@RequestMapping(value = "/webEmp", method = RequestMethod.GET)
	public String AdminHome(HttpSession session, HttpServletRequest request, Model model) throws Exception {

		List<AcctListDto> acctList = WebEmpService.getAcctCount();
		List<ApprovalDto> approvalList = WebEmpService.getAllApproval();
		
		int acctCount = acctList.size();
		int approvalCount = approvalList.size();
		int getAcctComList = WebEmpService.getAcctComList();
		int getApprovalComList = WebEmpService.getApprovalComList();
		
		int getUserlistDay = WebEmpService.getUserlistDay();
		int getUserlistWeek = WebEmpService.getUserlistWeek();
		int getUserlistMonth = WebEmpService.getUserlistMonth();
		
		int getLoanlistDay = WebEmpService.getLoanlistDay();
		int getLoanlistWeek = WebEmpService.getLoanlistWeek();
		int getLoanlistMonth = WebEmpService.getLoanlistMonth();
		
		int getAcctlistDay = WebEmpService.getAcctlistDay();
		int getAcctlistWeek = WebEmpService.getAcctlistWeek();
		int getAcctlistMonth = WebEmpService.getAcctlistMonth();
		
		HashMap<String, Integer> listData = new HashMap<String, Integer>();
		
		listData.put("getUserlistDay", getUserlistDay);
		listData.put("getUserlistWeek", getUserlistWeek);
		listData.put("getUserlistMonth", getUserlistMonth);
		
		listData.put("getLoanlistDay", getLoanlistDay);
		listData.put("getLoanlistWeek", getLoanlistWeek);
		listData.put("getLoanlistMonth", getLoanlistMonth);
		
		listData.put("getAcctlistDay", getAcctlistDay);
		listData.put("getAcctlistWeek", getAcctlistWeek);
		listData.put("getAcctlistMonth", getAcctlistMonth);
				
		model.addAttribute("listData", listData);
		model.addAttribute("getApprovalComList", getApprovalComList);
		model.addAttribute("getAcctComCount", getAcctComList);
		model.addAttribute("approvalCount", approvalCount);
		model.addAttribute("acctCount", acctCount);
		
		return "emp:dashBoard.tiles";
	}

	// [통장개설페이지]
	@RequestMapping(value = "/acctCreate", method = RequestMethod.GET)
	public String acctCreate(HttpSession session, HttpServletRequest request, Model model, @RequestParam(defaultValue = "1") int curPage) throws Exception {

		List<AcctListDto> count = WebEmpService.getAcctCount();
		int size = count.size();

		Pagination pagination = new Pagination(size, curPage);

		int index = pagination.getStartIndex();
		int pageSize = pagination.getPageSize();
		
		AcctListDto acctListDto = new AcctListDto(index, pageSize);
		List<AcctListDto> list = WebEmpService.getAcctList(acctListDto);

		model.addAttribute("pagination", pagination);
		model.addAttribute("acctList", list);
		
		return "emp:acctCreate.tiles";
	}

	// [통장등록처리]
	@RequestMapping(value = "/acctCreate", method = RequestMethod.POST)
	@ResponseBody
	public void saveAcct(@RequestBody AcctCreateDto acctData, HttpServletRequest request) throws Exception {

		WebEmpService.acctCreate(acctData);
	}

	// [대출미승인목록]
	@RequestMapping(value = "/loanRecognition", method = RequestMethod.GET)
	public String loanrecognition(HttpSession session, HttpServletRequest request, Model model, @RequestParam(defaultValue = "1") int curPage) throws Exception {

		List<ApprovalDto> approvalCount = WebEmpService.getAllApproval();
		int approvalSize = approvalCount.size();

		Pagination pagination = new Pagination(approvalSize, curPage);

		int index = pagination.getStartIndex();
		int pageSize = pagination.getPageSize();
				
		ApprovalDto approvalDto = new ApprovalDto(index, pageSize);

		List<ApprovalDto> list = WebEmpService.getApprovalData(approvalDto);

		model.addAttribute("pagination", pagination);
		model.addAttribute("appr", list);

		return "emp:loanRecognition.tiles";
	}

	// [대출승인처리]
	@RequestMapping(value = "/loanRecognition", method = RequestMethod.POST)
	@ResponseBody
	public void approvalProcess(@RequestBody RecognitionDto data) throws Exception {

		WebEmpService.depositMoney(data);
		WebEmpService.aprovalComplete(data);
	}

	// [대출상세]
	@RequestMapping(value = "/loanDetail", method = RequestMethod.GET)
	public String loanDetail(HttpSession session, HttpServletRequest request, Model model, @RequestParam(defaultValue = "1") int curPage) throws Exception {

		int getApprovalComList = WebEmpService.getApprovalComList();
		
		Pagination pagination = new Pagination(getApprovalComList, curPage);
		
		int index = pagination.getStartIndex();
		int pageSize = pagination.getPageSize();
		
		AcctListDto acctListDto = new AcctListDto(index, pageSize);
		
		List<ApprovalDto> getComApproval = WebEmpService.getComApproval(acctListDto);

		model.addAttribute("pagination", pagination);
		model.addAttribute("getComApproval", getComApproval);
		
		return "emp:loanDetail.tiles";
	}

	// [리포트]
	@RequestMapping(value = "/bankReport", method = RequestMethod.GET)
	public String bankReport(HttpSession session, HttpServletRequest request, Model model) throws Exception {

		List<StatisticsDto> list = WebEmpService.getStatData();
		
		model.addAttribute("stat", list);
		
		return "emp:bankReport.tiles";
	}

}
