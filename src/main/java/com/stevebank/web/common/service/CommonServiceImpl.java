package com.stevebank.web.common.service;


import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stevebank.web.common.dao.CommonDao;

@Service
public class CommonServiceImpl implements CommonService{
	
	
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public String testSql() {
		 String chk = "";
		 System.out.println("service오고있니?");
		 CommonDao commonDao = sqlSession.getMapper(CommonDao.class);
		 chk = commonDao.testSql();
		 System.out.println(1);
		 System.out.println("chk:" + chk);
		return chk;
	}

}
