package com.stevebank.web.webuser;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stevebank.web.common.HomeController;
import com.stevebank.web.webuser.dto.AccHistoryDto;
import com.stevebank.web.webuser.dto.AcctDto;
import com.stevebank.web.webuser.dto.ExDto;
import com.stevebank.web.webuser.dto.LoanMasterDto;
import com.stevebank.web.webuser.dto.UserDto;
import com.stevebank.web.webuser.service.WebuserServiceImpl;
import com.stevebank.web.webuser.sha256.ConvertSha256;

@Controller
public class SteveBankUserController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	// 웹유저 서비스
	@Autowired WebuserServiceImpl WebUserService;
	
	// 첫페이지
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpServletRequest request) 
	{
		if(request.getSession().getAttribute("acctinfo") == null)
		{
		String context = request.getContextPath();
		System.out.println(context);
		logger.info("Welcome home! The client locale is {}.", context);
		//session.setAttribute("context", context);
		//System.out.println("Cntrl  : "+CommonService.testSql());
		return "public:webuser/Firstpage.tiles";
		}
		else
		{
		return "public:webuser/Dashboardpage.tiles";
		}
	}
	
	// 회원가입 호출
	@RequestMapping(value="/join", method=RequestMethod.GET)
	public String join(HttpServletRequest request)
	{
		System.out.println("------가입페이지 호출-------");
		return "public:webuser/Joinpage.tiles";
	}
	
	// 회원가입처리 부분
	@RequestMapping(value="/join", method=RequestMethod.POST)
	public String RegisterJoin(HttpServletRequest request, UserDto userdto) throws Exception
	{
		
		System.out.println("-------회원가입 POST호출-------");
		userdto.setPw(new ConvertSha256().ConvertSha(userdto.getPw())); // 비밀번호 암호화처리
		WebUserService.userjoin(userdto); // 서비스 호출 db insert
		return "public:webuser/Firstpage.tiles"; // 처음페이지로 이동
	}
	
	// 아이디 중복체크 부분
	@RequestMapping(value="/check", method=RequestMethod.POST)
	@ResponseBody
	public String idcheck(@RequestParam("username") String data2, HttpServletRequest request)
	{
		System.out.println(data2);
		String CheckResult = WebUserService.usercheck(data2);
		return CheckResult;	
	}
	
	//비밀번호 찾기부분
	@RequestMapping(value="/find", method=RequestMethod.GET)
	public String findGet()
	{
		return "public:webuser/Findpage.tiles";
	}
	
	// 로그인 처리부분
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String LoginPrecess(HttpSession session,HttpServletRequest request, UserDto userdto) throws Exception
	{
		System.out.println("-------로그인 POST호출-------");
		userdto.setPw(new ConvertSha256().ConvertSha(userdto.getPw())); // 요청한 아이디 패스워드 비교를 위해 셋팅 
		UserDto LoginResult = WebUserService.logincheck(userdto); // 해당된 정보가 있으면 가져오고 못가져오면 null
		
		if(LoginResult == null) // 로그인없을경우
		{ 
			System.out.println("로그인 실패");
			return "public:webuser/Loginfailpage.tiles";
		}
		
		
		else if(LoginResult.getType().equals("c")) //관리자
		{
			System.out.println("관리자 로그인 성공");
			
			return "redirect:/webEmp/";
		}
		
		
		else if(LoginResult.getType().equals("e")) // 사용자
		{
			System.out.println("사용자 로그인 성공");
			AcctDto acctdto = WebUserService.getinfo(userdto.getId());
			session.setAttribute("acctinfo", acctdto);
			session.setAttribute("loginCheck", LoginResult);
			
			
			
			return "public:webuser/Dashboardpage.tiles";
		
		}
		else
		{
			System.out.println("로그인 실패");
			return "public:webuser/Loginfailpage.tiles";
		}	
	}
	
	// 로그아웃처리 세션처리
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String Logout(HttpSession session)
	{
		session.invalidate();
		return "redirect:/";
	}
	
	//입금페이지 
	@RequestMapping(value="/plus", method=RequestMethod.GET)
	public String plus(HttpSession session)
	{
		if(session.getAttribute("acctinfo") == null) // 페이지 막아주기
		{
		return "redirect:/";
		}
		else
		{
		return "public:webuser/Pluspage.tiles";
		}
	}
	
	//입금처리
	@RequestMapping(value="/plus", method=RequestMethod.POST)
	public String PlusProcess(HttpSession session,HttpServletRequest request, AcctDto acctdto) throws Exception
	{
		System.out.println("-------입금 프로세스 호출-------");
		System.out.println(acctdto.getBalance()+"원 입금요청됨");
		
		AcctDto dto = (AcctDto)session.getAttribute("acctinfo");
		dto.setBalance(dto.getBalance()+acctdto.getBalance()); // 값수정 Set하면 session값도 같이 바뀌네..?
		
		WebUserService.plus(dto);
		return "public:webuser/Dashboardpage.tiles";	
	}	
	
	//출금페이지 
		@RequestMapping(value="/minus", method=RequestMethod.GET)
		public String minus(HttpSession session)
		{
			if(session.getAttribute("acctinfo") == null) // 페이지 막아주기
			{
			return "redirect:/";
			}
			else
			{
			return "public:webuser/Minuspage.tiles";
			}
		}
		
		//출금처리
		@RequestMapping(value="/minus", method=RequestMethod.POST)
		public String MinusProcess(HttpSession session,HttpServletRequest request, AcctDto acctdto) throws Exception
		{
			System.out.println("-------출금 프로세스 호출-------");
			System.out.println(acctdto.getBalance()+"원 출금요청됨");
			
			AcctDto dto = (AcctDto)session.getAttribute("acctinfo");
			dto.setBalance(dto.getBalance()-acctdto.getBalance()); // 값수정 Set하면 session값도 같이 바뀌네..?
			
			WebUserService.plus(dto);
			return "public:webuser/Dashboardpage.tiles";	
		}	
		
		//계좌이체 페이지
		@RequestMapping(value="/trans", method=RequestMethod.GET)
		public String trans(HttpSession session)
		{
			System.out.println("계좌이체 페이지 호출");
			return "public:webuser/AcctTranspage.tiles";
		}
		
		
		//계좌이체 처리
		@RequestMapping(value="/trans", method=RequestMethod.POST, produces = "application/text; charset=utf8")
		@ResponseBody // 없으면 전달을못함.
		public String transProc(HttpSession session, HttpServletRequest request,ExDto exdto)
		{			
			String result = WebUserService.AcctNoCheck(exdto.getAcct_no());
			System.out.println(result);
			if(result == null)
			{	
				return "fail";
			}
			else
			{
				return result.toString();
			}
			
		}
		
		
		
		//계좌이체 처리2
		/*
		 * ****************************************************************************
		 * 
		 * 먼저 당행 타행을 구분 ( 데이터가 두개이기 때문에 따로 DB 접근하지않고 내부에서 if로 하드코딩 )
		 * 당행이면 스티브뱅크 
		 * 타행이면 DB에서 은행정보 코드 가져오기
		 * 타겟 계좌로 Plus
		 * 해당 계죄로 마이너스
		 * 
		 * 
		 * ****************************************************************************
		 * */
		
		@RequestMapping(value="/trans2", method=RequestMethod.POST)
		@ResponseBody // 없으면 전달을못함.
		public String transProc2(HttpSession session, HttpServletRequest request,ExDto exdto)
		{
		
			// 일단 내 정보를 가져옴
			AcctDto MyInfo = (AcctDto)session.getAttribute("acctinfo");
			AcctDto YouInfo = WebUserService.getinfo2(Integer.parseInt(exdto.getAcct_no()));
			
			if(exdto.getComm_code2().equals("당행이체"))
			{
				//당행이체 하드코딩
				AccHistoryDto historydto = new AccHistoryDto(Integer.parseInt(MyInfo.getAcct_no()),exdto.getAcct_no(),"001",exdto.getBalance(),MyInfo.getId(),"101");
				
				//계좌입금처리
				YouInfo.setBalance(YouInfo.getBalance()+Integer.parseInt(exdto.getBalance()));
				WebUserService.plus(YouInfo);
			
				
				// 내 계좌 출금처리 당행이체 출금 300원
				MyInfo.setBalance(MyInfo.getBalance()-Integer.parseInt(exdto.getBalance())-300);
				WebUserService.plus(MyInfo);
				
				//계좌히스토리에 인서트
				WebUserService.InsertHistory(historydto);
				return "sucess";
				
				
			}
			else
			{
				// 타행 은행코드 가져오기
				String GetBankCode = WebUserService.GetCommCode(exdto.getComm_code());
				// 타행이체 하드코딩
				AccHistoryDto historydto = new AccHistoryDto(Integer.parseInt(MyInfo.getAcct_no()),exdto.getAcct_no(),GetBankCode,exdto.getBalance(),MyInfo.getId(),"102");
				
				
				//계좌입금처리
				YouInfo.setBalance(YouInfo.getBalance()+Integer.parseInt(exdto.getBalance()));
				WebUserService.plus(YouInfo);
			
				
				// 내 계좌 출금처리 당행이체 출금 300원
				MyInfo.setBalance(MyInfo.getBalance()-Integer.parseInt(exdto.getBalance())-500);
				WebUserService.plus(MyInfo);
				
				//계좌히스토리에 인서트
				WebUserService.InsertHistory(historydto);
				
				return "suceess";		
			}
			
		}
		
		//대출신청페이지
		@RequestMapping(value="/loanregister",method=RequestMethod.GET)
		public String LoanRegister(HttpServletRequest request)
		{
			
			return "public:webuser/LoanRegister.tiles";
		}
		
		//대출신청페이지 프로세스
		@RequestMapping(value="/loanregister",method=RequestMethod.POST)
		public String LoanRegisterPoc(HttpSession session,HttpServletRequest request, LoanMasterDto loandto)
		{
			// 일단 내 정보를 가져옴
			AcctDto MyInfo = (AcctDto)session.getAttribute("acctinfo");
			
			System.out.println("대출신청 프로세스 호출");
			System.out.println(loandto.getPrt_code());
			System.out.println(loandto.getLoan_total());
			System.out.println(loandto.getLoan_month());
			
			String GetPrtCode = WebUserService.SelectPrtCode(loandto.getPrt_code());
			loandto.setPrt_code(GetPrtCode);
			loandto.setAcct_no(Integer.parseInt(MyInfo.getAcct_no()));
			loandto.setId(MyInfo.getId());
			
			WebUserService.InsertLoanRegister(loandto);
			
			return "";
		}
}
