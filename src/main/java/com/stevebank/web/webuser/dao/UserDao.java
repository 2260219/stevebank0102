package com.stevebank.web.webuser.dao;

import com.stevebank.web.webuser.dto.UserDto;

public interface UserDao {
	
	public void userjoin(UserDto userdto);
	public String usercheck(String mail);
	public UserDto logincheck(UserDto userdto);
	
}
