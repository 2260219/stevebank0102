package com.stevebank.web.webuser.dto;

public class UserDto {
	

	private String id;
	private String pw;
	private String name;
	private String sn;
	private String address;
	private String type;
	private String level;

	public UserDto(){};
	
	public UserDto(String id, String pw, String name, String usertype) {
		super();
		this.id = id;
		this.pw = pw;
		this.name = name;
		this.type = usertype;
	}
	
	public UserDto(String id, String name, String usertype) {
		super();
		this.id = id;
		this.name = name;
		this.type = usertype;
	}
	
	public UserDto(String id, String pw, String name, String sn, String address, String usertype, String level) {
		super();
		this.id = id;
		this.pw = pw;
		this.name = name;
		this.sn = sn;
		this.address = address;
		this.type = usertype;
		this.level = level;
	}

	public UserDto(String id, String pw, String name, String sn, String address, String usertype) {
		super();
		this.id = id;
		this.pw = pw;
		this.name = name;
		this.sn = sn;
		this.address = address;
		this.type = usertype;
	}
	
	public UserDto(String id, String pw) {
		super();
		this.id = id;
		this.pw = pw;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
