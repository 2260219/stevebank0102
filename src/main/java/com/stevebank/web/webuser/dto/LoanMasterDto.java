package com.stevebank.web.webuser.dto;

import java.util.Date;

public class LoanMasterDto {

	private String loan_seq; // 대출 일련번호
	private int acct_no; // 내 계좌
	private String prt_code; // 대출회사 코드
	private int loan_total; // 대출금액 
	private int loan_month; // 대출기간
	private int loan_balance; // 대출남은 금액
	private int loan_amount; // 월 상환원금
	private int loan_fee; // 월 상환 수수료
	private float loan_rate; // 대출 연체일
	private float monthly_payment; // 모름
	private int delay_cnt; // 연체 카운트
	private String id;
	private String acct_seq;
	private String approval;
	private Date loan_ask;
	private Date dead_dt;
	private Date acct_dt;
	
	public LoanMasterDto() {
		super();
	}
	public LoanMasterDto(String prt_code, int loan_total, int loan_month) {
		super();
		this.prt_code = prt_code;
		this.loan_total = loan_total;
		this.loan_month = loan_month;
	}
	public LoanMasterDto(String loan_seq, int acct_no, String prt_code, int loan_total, int loan_month, String id,
			String approval, Date loan_ask) {
		super();
		this.loan_seq = loan_seq;
		this.acct_no = acct_no;
		this.prt_code = prt_code;
		this.loan_total = loan_total;
		this.loan_month = loan_month;
		this.id = id;
		this.approval = approval;
		this.loan_ask = loan_ask;
	}
	public String getLoan_seq() {
		return loan_seq;
	}
	public void setLoan_seq(String loan_seq) {
		this.loan_seq = loan_seq;
	}
	public int getAcct_no() {
		return acct_no;
	}
	public void setAcct_no(int acct_no) {
		this.acct_no = acct_no;
	}
	public String getPrt_code() {
		return prt_code;
	}
	public void setPrt_code(String prt_code) {
		this.prt_code = prt_code;
	}
	public int getLoan_total() {
		return loan_total;
	}
	public void setLoan_total(int loan_total) {
		this.loan_total = loan_total;
	}
	public int getLoan_month() {
		return loan_month;
	}
	public void setLoan_month(int loan_month) {
		this.loan_month = loan_month;
	}
	public int getLoan_balance() {
		return loan_balance;
	}
	public void setLoan_balance(int loan_balance) {
		this.loan_balance = loan_balance;
	}
	public int getLoan_amount() {
		return loan_amount;
	}
	public void setLoan_amount(int loan_amount) {
		this.loan_amount = loan_amount;
	}
	public int getLoan_fee() {
		return loan_fee;
	}
	public void setLoan_fee(int loan_fee) {
		this.loan_fee = loan_fee;
	}
	public float getLoan_rate() {
		return loan_rate;
	}
	public void setLoan_rate(float loan_rate) {
		this.loan_rate = loan_rate;
	}
	public float getMonthly_payment() {
		return monthly_payment;
	}
	public void setMonthly_payment(float monthly_payment) {
		this.monthly_payment = monthly_payment;
	}
	public int getDelay_cnt() {
		return delay_cnt;
	}
	public void setDelay_cnt(int delay_cnt) {
		this.delay_cnt = delay_cnt;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAcct_seq() {
		return acct_seq;
	}
	public void setAcct_seq(String acct_seq) {
		this.acct_seq = acct_seq;
	}
	public String getApproval() {
		return approval;
	}
	public void setApproval(String approval) {
		this.approval = approval;
	}
	public Date getLoan_ask() {
		return loan_ask;
	}
	public void setLoan_ask(Date loan_ask) {
		this.loan_ask = loan_ask;
	}
	public Date getDead_dt() {
		return dead_dt;
	}
	public void setDead_dt(Date dead_dt) {
		this.dead_dt = dead_dt;
	}
	public Date getAcct_dt() {
		return acct_dt;
	}
	public void setAcct_dt(Date acct_dt) {
		this.acct_dt = acct_dt;
	}
	
	
	
	
}
