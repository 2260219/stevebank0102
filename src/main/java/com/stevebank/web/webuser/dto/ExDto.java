package com.stevebank.web.webuser.dto;

public class ExDto {
	
	private String acct_no;
	private String balance;
	private String comm_code;
	private String comm_code2;
	public ExDto() {}
	public ExDto(String acct_no, String balance, String comm_code, String comm_code2) {
		super();
		this.acct_no = acct_no;
		this.balance = balance;
		this.comm_code = comm_code;
		this.comm_code2 = comm_code2;
	}
	public String getAcct_no() {
		return acct_no;
	}
	public void setAcct_no(String acct_no) {
		this.acct_no = acct_no;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getComm_code() {
		return comm_code;
	}
	public void setComm_code(String comm_code) {
		this.comm_code = comm_code;
	}
	public String getComm_code2() {
		return comm_code2;
	}
	public void setComm_code2(String comm_code2) {
		this.comm_code2 = comm_code2;
	}
	


	
	

	
}
