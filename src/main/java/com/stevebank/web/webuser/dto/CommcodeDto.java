package com.stevebank.web.webuser.dto;

public class CommcodeDto {
	
	private String comm_seq;
	private String comm_type;
	private String comm_code;
	private String comm_name;
	private String comm_val;
	
	
	public CommcodeDto(String comm_seq, String comm_type, String comm_code, String comm_name, String comm_val) {
		super();
		this.comm_seq = comm_seq;
		this.comm_type = comm_type;
		this.comm_code = comm_code;
		this.comm_name = comm_name;
		this.comm_val = comm_val;
	}
	
	public String getComm_seq() {
		return comm_seq;
	}
	public void setComm_seq(String comm_seq) {
		this.comm_seq = comm_seq;
	}
	public String getComm_type() {
		return comm_type;
	}
	public void setComm_type(String comm_type) {
		this.comm_type = comm_type;
	}
	public String getComm_code() {
		return comm_code;
	}
	public void setComm_code(String comm_code) {
		this.comm_code = comm_code;
	}
	public String getComm_name() {
		return comm_name;
	}
	public void setComm_name(String comm_name) {
		this.comm_name = comm_name;
	}
	public String getComm_val() {
		return comm_val;
	}
	public void setComm_val(String comm_val) {
		this.comm_val = comm_val;
	}
	
	

}
