package com.stevebank.web.webuser.dto;

public class AcctDto {
	private String id;
	private String acct_no;
	private int balance;

	public AcctDto() {}
	
	public AcctDto(String id, String acct_no, int balance) {
		super();
		this.id = id;
		this.acct_no = acct_no;
		this.balance = balance;
	}

	public AcctDto(int balance) {
		super();
		this.balance = balance;
	}

	public AcctDto(String acct_no, int balance) {
		super();
		this.acct_no = acct_no;
		this.balance = balance;
	}

	public AcctDto(String acct_no) {
		super();
		this.acct_no = acct_no;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAcct_no() {
		return acct_no;
	}
	public void setAcct_no(String acct_no) {
		this.acct_no = acct_no;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
		
}
