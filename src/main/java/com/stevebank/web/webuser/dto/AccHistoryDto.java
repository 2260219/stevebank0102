package com.stevebank.web.webuser.dto;

public class AccHistoryDto {

	private int acct_no;
	private String acct_target;
	private String target_comp;
	private String amount;
	private String id;
	private String transfer_fee;
	
	
	public AccHistoryDto(int acct_no, String acct_target, String target_comp, String amount, String id,
			String transfer_fee) {
		super();
		this.acct_no = acct_no;
		this.acct_target = acct_target;
		this.target_comp = target_comp;
		this.amount = amount;
		this.id = id;
		this.transfer_fee = transfer_fee;
	}
	public int getAcct_no() {
		return acct_no;
	}
	public void setAcct_no(int acct_no) {
		this.acct_no = acct_no;
	}
	public String getAcct_target() {
		return acct_target;
	}
	public void setAcct_target(String acct_target) {
		this.acct_target = acct_target;
	}
	public String getTarget_comp() {
		return target_comp;
	}
	public void setTarget_comp(String target_comp) {
		this.target_comp = target_comp;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTransfer_fee() {
		return transfer_fee;
	}
	public void setTransfer_fee(String transfer_fee) {
		this.transfer_fee = transfer_fee;
	}
	
	
	
	
}
