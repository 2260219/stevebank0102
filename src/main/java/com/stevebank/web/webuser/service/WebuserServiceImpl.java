package com.stevebank.web.webuser.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stevebank.web.webuser.dao.AcctDao;
import com.stevebank.web.webuser.dao.UserDao;
import com.stevebank.web.webuser.dto.AccHistoryDto;
import com.stevebank.web.webuser.dto.AcctDto;
import com.stevebank.web.webuser.dto.LoanMasterDto;
import com.stevebank.web.webuser.dto.UserDto;

@Service
public class WebuserServiceImpl implements WebuserService {
	
	@Autowired
	private SqlSession session;
	
	@Override
	public void userjoin(UserDto userdto)
	{
		UserDao userdao = session.getMapper(UserDao.class);
		userdao.userjoin(userdto);
		System.out.println("------회원가입 서비스 호출------");
	}
	
	@Override
	public String usercheck(String mail)
	{
		UserDao userdao = session.getMapper(UserDao.class);
		String check = userdao.usercheck(mail);
		if(check==null)
		{
			System.out.println("가져온 아이디값 : "+check);	
			return "sucess";
		}
		else
		{
			System.out.println("가져온 아이디값 : "+check);	
			return "fail";
		}
		
	}
	
	@Override
	public UserDto logincheck(UserDto userdto)
	{
		UserDao userdao = session.getMapper(UserDao.class);
		System.out.println("------로그인체크 메소드 호출-------");
		UserDto ResultDto = userdao.logincheck(userdto);
		return ResultDto;
	}
	
	@Override
	public AcctDto getinfo(String id)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		System.out.println("-------계좌정보 조회 메소드-------");
		AcctDto ResultDto = acctdao.getinfo(id);
		if(ResultDto==null)
		{
			System.out.println("계좌정보없음");
			return null;	
		}
		else
		{
			System.out.println(ResultDto.getAcct_no());
			System.out.println(ResultDto.getBalance());
			System.out.println(ResultDto.getId());
			return ResultDto;
		}
	}
	
	@Override
	public AcctDto getinfo2(int acct_no)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		System.out.println("-------계좌정보 조회 메소드-------");
		AcctDto ResultDto = acctdao.getinfo2(acct_no);
		if(ResultDto==null)
		{
			System.out.println("계좌정보없음");
			return null;	
		}
		else
		{
			System.out.println(ResultDto.getAcct_no());
			System.out.println(ResultDto.getBalance());
			return ResultDto;
		}
	}
	
	@Override
	public void plus(AcctDto acctdto)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		acctdao.plus(acctdto); 
	}
	
	@Override
	public String AcctNoCheck(String acct_no)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		String result = acctdao.AcctNoCheck(acct_no);
		
		return result;
	}
	
	@Override
	public String GetCommCode(String comm_name)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		String result = acctdao.GetCommCode(comm_name);
		
		return result;
	}
	
	@Override
	public void InsertHistory(AccHistoryDto historydto)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		acctdao.InsertHistory(historydto);
		
	}
	
	@Override
	public String SelectPrtCode(String prt_name)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		String result = acctdao.SelectPrtCode(prt_name);
		return result;
	}
	
	@Override
	public void InsertLoanRegister(LoanMasterDto loandto)
	{
		AcctDao acctdao = session.getMapper(AcctDao.class);
		acctdao.InsertLoanRegister(loandto);
	}

}
