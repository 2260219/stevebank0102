package com.stevebank.web.webuser.service;

import com.stevebank.web.webuser.dto.AccHistoryDto;
import com.stevebank.web.webuser.dto.AcctDto;
import com.stevebank.web.webuser.dto.LoanMasterDto;
import com.stevebank.web.webuser.dto.UserDto;

public interface WebuserService {
	
	// UserDao & Dto
	public void userjoin(UserDto userdto);
	public String usercheck(String mail);
	public UserDto logincheck(UserDto userdto);
	
	// AcctDao & Dto
	public AcctDto getinfo(String id);
	public AcctDto getinfo2(int acct_no);
	public void plus(AcctDto acctdto);
	public String AcctNoCheck(String acct_no);
	public String GetCommCode(String comm_name);
	public void InsertHistory(AccHistoryDto historydto);
	public String SelectPrtCode(String prt_name);
	public void InsertLoanRegister(LoanMasterDto loandto);
	

	
}
