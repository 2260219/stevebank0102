package com.stevebank.web.webuser.sha256;

import java.security.MessageDigest;

//암호화 프로세스
public class ConvertSha256 {
	
	public String ConvertSha(String pwd) throws Exception
	{
		System.out.println("-----pwd암호화 프로세스 호출-------");
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(pwd.getBytes("UTF-8"));
		StringBuffer hexString = new StringBuffer();
		
		for(int i=0;i<hash.length;i++)
		{
			String hex=Integer.toHexString(0xff & hash[i]);
			if(hex.length()==1) hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}
	
}
