package com.stevebank.batchkim.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stevebank.batchkim.dao.BatchDao;

@Service
public class BatchServiceImpl implements BatchService {
	@Autowired
	private SqlSession sqlSession;

	@Override
	public void totalfeeProcedure() {
		BatchDao batchDao = sqlSession.getMapper(BatchDao.class);		
		batchDao.totalfeeProcedure();
	}

}
