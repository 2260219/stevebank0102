package com.stevebank.batchkim;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import com.stevebank.batchkim.service.BatchServiceImpl;

@Controller
public class TaskBatchController {
	
	@Autowired
	BatchServiceImpl batchService;

//	@Scheduled(cron = "*/20 * * * * *")
//	매일 오전 1시 30분
	@Scheduled(cron = "0 30 1 * * *")
	public void feeProcCall() {
		batchService.totalfeeProcedure();
	}
}
